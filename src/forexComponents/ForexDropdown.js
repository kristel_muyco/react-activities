import React, { useState } from "react";
import {
  FormGroup,
  Label,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import Currencies from "./ForexData";

function ForexDropdown(props) {
  //useState
  const [dropdownIsOpen, setDropdownIsOpen] = useState(false);
  const [currency, setCurrency] = useState(null);

  return (
    <FormGroup>
      <Label>{props.label}</Label>
      <Dropdown
        isOpen={dropdownIsOpen}
        toggle={() => setdropdownIsOpen(!dropdownIsOpen)}
      >
        <DropdownToggle caret>
          {!props.currency ? "Choose Currency" : props.currency.currency}
        </DropdownToggle>
        <DropdownMenu>
          {Currencies.map((currency, index) => (
            <DropdownItem key={index} onClick={() => props.onClick(currency)}>
              {currency.currency}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </Dropdown>
    </FormGroup>
  );
}

export default ForexDropdown;
