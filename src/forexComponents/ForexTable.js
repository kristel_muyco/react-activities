import React from "react";
import { Table } from "reactstrap";

const ForexTable = props => {
  return (
    <Table className="striped border">
      <thead>
        <tr>
          <th>Currency</th>
          <th>Rate</th>
        </tr>
      </thead>
      <tbody></tbody>
    </Table>
  );
};

export default ForexTable;
