const Currencies = [
  {
    currency: "Philippine Peso",
    code: "PHP"
  },
  {
    currency: "US Dollars",
    code: "USD"
  },
  {
    currency: "Euro",
    code: "EUR"
  },
  {
    currency: "Thai Baht",
    code: "THB"
  },
  {
    currency: "South Korean Won",
    code: "KRW"
  },
  {
    currency: "British Pound",
    code: "GPB"
  },
  {
    currency: "Japanese Yen",
    code: "JPY"
  },
  {
    currency: "Indian Rupee",
    code: "INR"
  }
];

export default Currencies;
