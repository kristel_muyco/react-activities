import React, { useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  Label,
  Input,
  Button
} from "reactstrap";

const CrudForm = props => {
  const [question, setQuestion] = useState("");
  const [category, setCategory] = useState("");

  return (
    <Modal isOpen={props.showForm} toggle={props.toggleForm}>
      <ModalHeader toggle={props.toggleForm}>
        {props.isEditing ? "Edit Question" : "Add Question"}
      </ModalHeader>
      <ModalBody>
        <FormGroup>
          <Label>Question:</Label>
          <Input
            placeholder="Type your question"
            onChange={e => setQuestion(e.target.value)}
            defaultValue={props.questionToEdit.question}
          />
        </FormGroup>
        <FormGroup>
          <Label>Category:</Label>
          <Input
            placeholder="Type your category"
            onChange={e => setCategory(e.target.value)}
            defaultValue={props.questionToEdit.category}
          />
        </FormGroup>
        <Button
          color="success"
          onClick={() => props.saveQuestion(question, category)}
        >
          {props.isEditing ? "Edit Question" : "Add Question"}
        </Button>
      </ModalBody>
    </Modal>
  );
};

export default CrudForm;
