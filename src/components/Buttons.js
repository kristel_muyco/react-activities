import React, { Component } from "react";

import Button from "./Button";

class Buttons extends Component {
  render() {
    return (
      <div
        className="d-flex justify-content-around"
        style={{
          margin: "0 200px"
        }}
      >
        <Button
          color={"btn-info"}
          text={"+1"}
          //Step 3: Pass the function we received as a property
          //to Button Component
          handleonClick={this.props.handleAdd}
        />
        <Button
          color={"btn-warning"}
          text={"-1"}
          handleonClick={this.props.handleMinus}
        />
        <Button
          color={"btn-success"}
          text={"Reset"}
          handleonClick={this.props.handleReset}
        />
        <Button
          color={"btn-secondary"}
          text={"x2"}
          handleonClick={this.props.handleMultiply}
        />
      </div>
    );
  }
}

export default Buttons;
