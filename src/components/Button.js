import React, { Component } from "react";

class Button extends Component {
  //props or property
  render() {
    return (
      <button
        className={this.props.color}
        //Step 4: Use the function we received as a property
        //in our event listener propert (onClick)
        onClick={this.props.handleonClick}
      >
        {this.props.text}
      </button>
    );
  }
}

export default Button;
