import React, {Component} from 'react';
//import box component
import Box from "./components/Box";
//import buttons component
import Buttons from "./components/Buttons";


class CalculatorApp extends Component {

    handleAdd = () => {
        //Step 5: Set up the set state function (or change the state of count)
        //based on the role of the button
        this.setState({
          count: this.state.count + 1
        });
      };
    
      handleMinus = () => {
        this.setState({
          count: this.state.count - 1
        });
      };
    
      handleReset = () => {
        this.setState({
          count: 0
        });
      };
    
      handleMultiply = () => {
        this.setState({
          count: this.state.count * 2
        });
      };

    render(){
        return();
    }
}

export default CalculatorApp;