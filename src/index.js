import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

// This is the counter app;
//import App from "./App";

//This is the Forex App
// import ForexApp from "./ForexApp";

import CRUDApp from "./CRUDApp";

import * as serviceWorker from "./serviceWorker";

//bootstrap
import "bootstrap/dist/css/bootstrap.css";

ReactDOM.render(
  <React.StrictMode>
    <CRUDApp />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
